function setColor(){
    const rgbColor = `rgb(${getRandomNumber(255)}, ${getRandomNumber(255)}, ${getRandomNumber(255)})`;

    backgroundElement.style = `background-color:${rgbColor}`;
    textColorElement.innerText = rgbColor;
}

btnColorElement.addEventListener("click", setColor);